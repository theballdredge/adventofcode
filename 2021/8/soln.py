def decode(map, rhs):
    retval=[]
    for value in rhs:
        retval.append(str(map[''.join(value)]))
    return ''.join(retval)

def solve(lhs,rhs):
    mapping=[0]*10
    wiring=[0]*10
    
    for wire in lhs:
        if len(wire) == 2:
            mapping[1] = wire
        if len(wire) == 3:
            mapping[7] = wire
        if len(wire) == 4:
            mapping[4] = wire
        if len(wire) == 7:
            mapping[8] = wire
    for c in mapping[7]:
        if c in mapping[1]:
            next
        else:
            wiring[0]=c
    for wire in lhs:
        if len(wire) == 5:
            if all([c in wire for c in mapping[7]]):
                mapping[3] = wire

    for wire in lhs:
        if len(wire) == 6:            
            if all([c in sorted(list(set(mapping[3])|set(mapping[4]))) for c in wire]):
                mapping[9]=wire
    for c in mapping[3]:
        if c in mapping[7]:
            next
        elif c in mapping [4]:
            next
        else:
            wiring[6]=c
            # print(c)
    for c in mapping[3]:
        if c in mapping[7]:
            next
        elif c == wiring[6]:
            next
        else:
            wiring[3]=c
    for wire in lhs:
        if len(wire) == 6:
            if wiring[3] not in wire:
                mapping[0]=wire
    for wire in lhs:
        if len(wire) == 6:
            if wire == mapping[9]:
                next
            elif wire == mapping[0]:
                next
            else:
                mapping[6]=wire
    for wire in lhs:
        if len(wire) == 5:
            if wire == mapping[3]:
                next
            else:
                if all([c in mapping[6] for c in wire]):
                    mapping[5]=wire
                else:
                    mapping[2]=wire
    retval={}
    for element in mapping:
        retval.update({''.join(element):  mapping.index(element)})
    return decode(retval,rhs)

with open('test','r') as f:
    line=f.readline().strip()
    
    total = 0
    while line:
        lhs,rhs=line.split("|")
        
        lhs_arr=[sorted(x) for x in lhs.strip().split(' ')]
        rhs_arr=[sorted(x) for x in rhs.strip().split(' ')]
        total+=int(solve(lhs_arr, rhs_arr))

       
        line=f.readline().strip()
    print(total)
    if total != 61229:
        print('error ',total)
        exit()

with open('input','r') as f:
    line=f.readline().strip()
    
    total = 0
    while line:
        lhs,rhs=line.split("|")
        
        lhs_arr=[sorted(x) for x in lhs.strip().split(' ')]
        rhs_arr=[sorted(x) for x in rhs.strip().split(' ')]
        total+=int(solve(lhs_arr, rhs_arr))
       
        line=f.readline().strip()
    print(total)