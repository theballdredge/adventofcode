from pprint import pprint
from typing import final
import collections

def setup(file):
    with open(file,'r') as f:
        # map=[]
        line=f.readline().strip()
        # while line:
        #     map.append([x for x in line.strip()])
        #     line=f.readline().strip()
    return line
#p1 test

def decode(packet):
    return str(bin(int(packet,16)))[2:]

def process(packet):
    print(packet)
    version=int(packet[0:3],2)
    type=int(packet[3:6],2)
    # print(int(type,2))

    if type == 4:
        remaining=len(packet[6:])
        data=packet[6:]
        for x in range(remaining%4):
            data+='0'
        print('data',data,len(data))
        val_len=int(len(data)/4)
        flag=data[0]
        valstring=""
        while int(flag) == 1:
            flag=data[0]
            valstring+=data[1:val_len]
            data=data[val_len:]
        print('valstring',int(valstring,2))
        return int(valstring,2)
    else:
        if type == 0:
            print(packet[6:21])
            length=int(packet[6:20],2)
            print('length',length)
            return length
    
def run(file,testing):
    packet=setup(file)
    decoded=decode(packet)
    score=process(decoded)
    if testing > 0:
        if score != testing:
            print('testing error ',testing,score)
            exit()
        else:
            print('test success')
    else:
        highlist=[0]
        lowlist=[0]
        wronglist=[2437]
        if score in wronglist:
            print('nope',score)
        elif highlist[0] == 0 or score in range(max(lowlist),min(highlist)):
            print('maybe',score)
        else:
            print('nope',score)


run('test',2021)
run('test2',27)
run('input',-1)
exit()
run('test',2188189693529,40)

run('input',-1,40)
