w, h = 1001, 1001
map = [[0] * w for i in range(h)]
v=[]
count_i=0
with open('input','r') as f:
    line=f.readline()
    while line:
        p1,p2 = line.split('->')
        x1,y1 = (int(x) for x in p1.strip().split(','))
        x2,y2 = (int(x) for x in p2.strip().split(','))
        
        newVector={'b':{'x':x1, 'y':y1},'e':{'x':x2,'y':y2}}
        v.append(newVector)
        line=f.readline()  
for vector in v:
    if vector['b']['x']==vector['e']['x']:
        x=vector['b']['x']
        if vector['b']['y'] < vector['e']['y']:
            step=1
        else:
            step=-1
        for y in range(vector['b']['y'],vector['e']['y']+step,step):
            map[x][y]+=1
    elif vector['b']['y']==vector['e']['y']:
        y=vector['b']['y']
        if vector['b']['x'] < vector['e']['x']:
            step=1
        else:
            step=-1
        for x in range(vector['b']['x'],vector['e']['x']+step,step):
            map[x][y]+=1
    else:
        if vector['b']['x'] < vector['e']['x']:
            xstep=1
        else:
            xstep=-1
        if vector['b']['y'] < vector['e']['y']:
            ystep=1
        else:
            ystep=-1
        xpos = vector['b']['x']
        ypos = vector['b']['y']
        while xpos != vector['e']['x']+xstep:
            map[xpos][ypos]+=1
            xpos+=xstep
            ypos+=ystep

count = 0
for line in map:
    for point in line:
        if point >= 2:
            count+=1
print(count)