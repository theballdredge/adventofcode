import pprint



def simulate(days,full_school,school=[1]):
    result=[]
    for i in range(0,days):
        counts = [0] * 9
        new_school = []
        print('day ',i,len(school))
        # print(school)
        for j in range(0,9):
            counts[j]=school.count(j)
        new_school=new_school+([8] * counts[0])
        new_school=new_school+([7] * counts[8])
        new_school=new_school+([6] * (counts[0]+counts[7]))
        new_school=new_school+([5] * counts[6])
        new_school=new_school+([4] * counts[5])
        new_school=new_school+([3] * counts[4])
        new_school=new_school+([2] * counts[3])
        new_school=new_school+([1] * counts[2])
        new_school=new_school+([0] * counts[1])
        school = new_school
        result.insert(0,len(school))
        if len(result) > 5:
            result.pop(5)
    print(result)
        
    total=0
    for i in full_school:
        total+=result[i-1]
    return(total)

def simulate_number(days,school=[1]):
    result=[]
    for i in range(0,days):
        counts = [0] * 9
        new_school = []
        for j in range(0,9):
            counts[j]=school.count(j)
        new_school=new_school+([8] * counts[0])
        new_school=new_school+([7] * counts[8])
        new_school=new_school+([6] * (counts[0]+counts[7]))
        new_school=new_school+([5] * counts[6])
        new_school=new_school+([4] * counts[5])
        new_school=new_school+([3] * counts[4])
        new_school=new_school+([2] * counts[3])
        new_school=new_school+([1] * counts[2])
        new_school=new_school+([0] * counts[1])
        school = new_school
    return(len(school),school)

with open('input','r') as f:
    line=f.readline().strip()
    input_school=[int(x) for x in line.strip().split(',')]
    simresult=simulate(80,input_school)
    print(simresult)
    if simresult != 355386:
        print('incorrect')
        exit()
    result_arr = [0] * 9
    school_arr = [[]]*9
    for i in range(0,9):
        result_arr[i],school_arr[i]=simulate_number(128,[i])
    total = 0
    midway_school=[]
    full_school=[]
    for fish in input_school:
        midway_school+=school_arr[fish]
    print('finished midway',len(midway_school))
    for fish in midway_school:
        total+=len(school_arr[fish])

    print(total)
