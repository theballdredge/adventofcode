def isminima(x,y,map):
    y_minus=True
    y_plus=True
    x_minus=True
    x_plus=True
    if y > 0:
        if map[x][y]>=map[x][y-1]:
            y_minus=False
    if y+1 < len(map[x]):
        if map[x][y]>map[x][y+1]:
            y_plus=False
    if x > 0:
        if map[x][y]>=map[x-1][y]:
            x_minus=False
    if x+1 < len(map):
        if map[x][y]>=map[x+1][y]:
            x_plus=False
    return y_minus & y_plus & x_minus & x_plus,x,y

def candidate_point(x,y,map):
    y_valid=False
    x_valid=False
    # print(x,y,len(map),len(map[0]))
    if y in range(0,len(map[0])):
        y_valid=True
    if x in range(0,len(map)):
        # print('valid x',x,len(map))
        x_valid=True

    if not (x_valid & y_valid):
        if x in range(0,len(map)) and y in range(0,len(map[0])):        
            print('invalid',x,y)
            print('*** fishy')
            exit()
    return x_valid & y_valid

def basin_size(x,y,map):
    points_to_check=[]
    checked=[]
    raw_checked=[]
    basin_points=[[x,y]]
    if candidate_point(x+1,y,map):
        points_to_check.append([[x+1,y],[x,y]])
    if candidate_point(x-1,y,map):
        points_to_check.append([[x-1,y],[x,y]])
    if candidate_point(x,y+1,map):
        points_to_check.append([[x,y+1],[x,y]])
    if candidate_point(x,y-1,map):
        points_to_check.append([[x,y-1],[x,y]])
    while points_to_check:
        testing = points_to_check.pop(0)
        cand_point,adj=testing
        c_x,c_y = cand_point
        adj_x, adj_y = adj
        checked.append(testing)
        raw_checked.append(cand_point)
        # if map[c_x][c_y] in range(map[adj_x][adj_y]-1,min(9,map[adj_x][adj_y]+2)):
        if map[c_x][c_y] != 9:
                if cand_point not in basin_points:
                    basin_points.append(cand_point)
                if candidate_point(c_x+1,c_y,map):
                    if [c_x+1,c_y] not in basin_points:
                        points_to_check.append([[c_x+1,c_y],cand_point])
                if candidate_point(c_x-1,c_y,map):
                    if [c_x-1,c_y] not in basin_points:
                        points_to_check.append([[c_x-1,c_y],cand_point])
                if candidate_point(c_x,c_y+1,map):
                    if [c_x,c_y+1] not in basin_points:
                        points_to_check.append([[c_x,c_y+1],cand_point])
                if candidate_point(c_x,c_y-1,map):
                    if [c_x,c_y-1] not in basin_points:
                        points_to_check.append([[c_x,c_y-1],cand_point])
    print(len(basin_points),basin_points)
    draw=False
    if draw:
        with open('output','a+') as f:
            for x in range(0,len(map)):
                # print(x,end='')
                for y in range (0,len(map[0])):
                
                    if [x,y] in basin_points:
                        f.write( str(map[x][y]))
                    elif [x,y] in raw_checked:
                        f.write( str(map[x][y]))
                    else:
                        f.write('.')
                f.write('\n')
            f.write('\n')
    return len(basin_points)
            


def solve(map):
    # print(map)
    value=[]
    for x in range(len(map)):
        for y in range(len(map[x])):
            is_minima,x_pos,y_pos=isminima(x,y,map)
            if is_minima:
                basin_size_val = basin_size(x,y,map)
                if len(value) < 3:
                    value.append(basin_size_val)
                else:
                    value.append(basin_size_val)
                    value=sorted(value,reverse=True)
                    # value.pop()
                # value+=int(map[x][y])+1
    print(value)
    retval = 1
    for basin in value:
        retval *= basin
    return value[0]*value[1]*value[2]
    
map=[]
with open('test','r') as f:
    line=f.readline().strip()
    
    # lineno=0
    while line:

        map.append([int(x) for x in line])
        # lineno+=1       
        line=f.readline().strip()
    
    # print(map)
    total = solve(map)
    print(total)
    testing = 1134
    if total != 1134:
        print('error ',testing,total)
        exit()

map=[]
with open('input','r') as f:
    line=f.readline().strip()
    
    # lineno=0
    while line:
        map.append([int(x) for x in line])
        # lineno+=1       
        line=f.readline().strip()
    
    # print(map)
    total = solve(map)

    print(total)
    if total in [807942]:
        print('wrong')