x = 0
y = 0
with open('input','r') as f:
    line = f.readline()
    while line:
        dir,mag_s = line.split(' ')
        mag=int(mag_s)
        if dir == 'forward':
            y = y + mag
        elif dir == 'up':
            x = x - mag
        elif dir == 'down':
            x = x + mag
        line = f.readline()

print(x*y)