x = 0
y = 0
aim = 0
with open('input','r') as f:
    line = f.readline()
    while line:
        dir,mag_s = line.split(' ')
        mag=int(mag_s)
        if dir == 'forward':
            y = y + mag
            x = x + mag * aim
        elif dir == 'up':
            aim = aim - mag
        elif dir == 'down':
            aim = aim + mag
        line = f.readline()

print(x*y)