from pprint import pprint
from typing import final

def fold(dir,spot,points):
    # print(dir,spot)
    newpoints=[]
    unique=[]
    count=0
    if dir == 'x':
        pivot=0
        for point in points:
            count+=1
            if point[0]>spot:
                newx=spot-(point[0]-spot)
                # print('folding',point,'on',spot,'to',[newx,point[1]])
                newpoints.append([newx,point[1]])
            else:
                newpoints.append(point)
    else:
        pivot=1
        for point in points:
            # print(point,spot)
            count+=1
            if point[1]>spot:
                newy=spot-(point[1]-spot)
                # print('folding',point,'on',spot,'to',[point[0],newy],point[1]-spot)
                
                newpoints.append([point[0],newy])
            else:
                # print('append',point)
                
                newpoints.append(point)
    for point in newpoints:
        if point not in unique:
            unique.append(point)
            if point[pivot]>spot:
                print('error')
        # else:
        #     print('dup not added',point)
    # print(count,len(newpoints),len(unique),len(points))
    return unique

def process(points,folds):
    # print(points,folds[0])
    for action in folds:
        dir,spot = action.split('=')
        # print(dir,spot)
        points=fold(dir,int(spot),points)
        # print(points)
        for y in range(6):
            for x in range(80):
                if [x,y] in points:
                    print('#',end='')
                else:
                    print(' ',end='')
            print()
    # dir,spot = folds[0].split('=')
    return points

def setup(file):
    with open(file,'r') as f:
        line=f.readline().strip()
        points=[]
        folds=[]
        while line:
            if ',' in line:
                x,y=line.split(',')
                points.append([int(x),int(y)])
            if '=' in line:
                folds.append(line.split()[3])
            line=f.readline().strip()
        
        line=f.readline().strip()
        while line:
            if ',' in line:
                x,y=line.split(',')
                points.append([x,y])
            if '=' in line:
                folds.append(line.split()[2])
            # print(folds)
            line=f.readline().strip()
    return points,folds
#p1 test

def run(file,testing):
    points,folds=setup(file)
    # print(points,folds)
    finalpoints=process(points,folds)
    # print(finalpoints)
    # for x in range(20):
    #     for y in range(0):
    #         if [x,y] in finalpoints:
    #             print('#',end='')
    #         else:
    #             print('.',end='')
    #     print()
    score=len(finalpoints)
    # print(score,testing)
    if testing > 0:
        if score != testing:
            print('testing error ',testing,score)
            exit()
        else:
            print('test success')
    else:
        highlist=[908]
        lowlist=[0]
        if score in range(max(lowlist),min(highlist)):
            print('maybe',score)
        else:
            print('nope')


run('test',16)

run('input',-1)
