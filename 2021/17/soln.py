from pprint import pprint
from typing import final

def setup(file):
    with open(file,'r') as f:
        line=f.readline().strip()
        foo,x_dirty,y_dirty=line.strip().split(':')[1].split(' ')
        x1,x2=[int(x) for x in x_dirty.strip(',').split('=')[1].split('..')]
        y1,y2=[int(x) for x in y_dirty.split('=')[1].split('..')]
    return [x1,x2,y1,y2]

def hitdetect(shot,target,v,e):
    if shot[0] in range(target[0],target[1]+1) and shot[1] in range(target[2],target[3]+1):
        return 'hit'
    # if target[2]>=0:
    if shot[0] > max(target[0],target[1]) or (shot[1] < min(target[2],target[3]) and shot[1]+e < min(target[2],target[3])):
        return 'miss'

    return 'pre'

def fire(target,v,e):
    max=0
    shot=[0,0]
    step=hitdetect(shot,target,v,e)
    while step=='pre':
        if shot[1]>max:
            max=shot[1]
        shot[0]+=v
        shot[1]+=e
        if v > 0:
            v-=1
        elif v < 0:
            v+=1
        e-=1 
        step=hitdetect(shot,target,v,e)
    if step=='hit':
        return(max,step)
    return 0,'false'


def process(target,limit):
    max=0
    hitlist=[]
    
    for e in range(int(limit/2)*-1,limit):
        for v in range(0,target[1]+2):
            score,status=fire(target,v,e)
            if status=='hit':
                # print('hit',[v,e])
                hitlist.append([v,e])
            if score>max:
                max=score
            # if e%1000 == 0:
            #     print([e,v])
    return max,len(hitlist)

    
def run(file,testing,hittest,limit):
    target=setup(file)
    score,numhits=process(target,limit)
    if testing > 0 and hittest > 0:
        if score != testing and hittest != numhits:
            print('testing error ',testing,score,numhits,hittest)
            exit()
        else:
            print('test success')
    else:
        highlist=[0]
        lowlist=[321]
        wronglist=[321, 998, 1010, 2234, 2188]
        if score != testing or numhits in wronglist:
            print('nope',score,numhits)
        elif score == testing and hittest not in wronglist:
            print('maybe',score,numhits)
        else:
            print('nope',score,numhits)


run('test',45,112,100)
# exit()
run('input',14535,-1,1000)
# exit()
# run('test')

# run('input',-1)
