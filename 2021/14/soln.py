from pprint import pprint
from typing import final
import collections

def setup(file):
    with open(file,'r') as f:
        template=f.readline().strip()
        f.readline()
        line=f.readline().strip()
        pairs=[]
        while line:
            pairs.append(line.strip())
            line=f.readline().strip()
    return template,pairs
#p1 test

def expand(template,pairs,count):
    paircount={}
    # print(template)
    for c in range(len(template)-1):
        pair=template[c]+template[c+1]
        if pair not in paircount.keys():
            paircount[pair]=0
        paircount[pair]+=1
        if c == 0:
            startchar = pair[0]
    endchar = pair[1]
    for i in range(count):
        newpaircount={}
        nextstep=[]
        for pair in paircount.keys():
            # print(pair,paircount[pair])
            np1=pair[0]+pairs[pair]
            np2=pairs[pair]+pair[1]
            if np1 not in newpaircount.keys():
                newpaircount[np1]=0
            if np2 not in newpaircount.keys():
                newpaircount[np2]=0
            newpaircount[np1]+=paircount[pair]
            newpaircount[np2]+=paircount[pair]
        paircount=newpaircount.copy()

    return paircount

def process(paircount,template):
    unique=[]
    for pair in paircount.keys():
        for c in pair:
            if c not in unique:
                unique.append(c)
    counts={}
    min=0
    max=0
    total=0
    for c in unique:
        counts[c]=0
        for pair in paircount.keys():
            if c == pair[0]:
                counts[c]+=paircount[pair]
            if c == pair[1]:
                counts[c]+=paircount[pair]
        if c in [template[0],template[-1]]:
            counts[c]+=1
        counts[c]=int(counts[c]/2)
        if min == 0:
            min = counts[c]
        if counts[c]<min:
            min = counts[c]
        if counts[c]>max:
            max = counts[c]
        total+=counts[c]
    return max-min

    
def run(file,testing,count):
    template,pairs_raw=setup(file)
    pairs={}
    for pair in sorted(pairs_raw):
        start,finish=pair.split(' -> ')
        pairs[start]=finish
    score=process(expand(template,pairs,count),template)
    if testing > 0:
        if score != testing:
            print('testing error ',testing,score)
            exit()
        else:
            print('test success')
    else:
        highlist=[0]
        lowlist=[0]
        wronglist=[2437]
        if score in wronglist:
            print('nope',score)
        elif highlist[0] == 0 or score in range(max(lowlist),min(highlist)):
            print('maybe',score)
        else:
            print('nope',score)


run('test',1588,10)

run('input',2703,10)

run('test',2188189693529,40)

run('input',-1,40)
