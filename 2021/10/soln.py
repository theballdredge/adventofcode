def complete(line):
    openers = ['(','{','<','[']
    closers = [')','}','>',']']
    pairs = {'(':')','[':']','<':'>','{':'}'}
    values = {')':1,']':2,'}':3,'>':4}
    stack = []
    completion_stack = []
    score=0
    for c in line:
        if c in openers:
            stack.append(c)
        if c in closers:
            stack.pop()
    while stack:
        c=stack.pop()
        completion_stack.append(pairs[c])
    
    for c in completion_stack:
        score=score*5+values[c]
    return score

def is_corrupt(line):
    openers = ['(','{','<','[']
    closers = [')','}','>',']']
    pairs = {'(':')','[':']','<':'>','{':'}'}
    values = {')':3,']':57,'}':1197,'>':25137}
    stack = []
    for c in line:
        if c in openers:
            stack.append(c)
        if c in closers:
            check = stack.pop()
            if c != pairs[check]:
                print(line,check,pairs[check],c)
                return values[c]
    return 0
def solve(input):
    return 1

with open('test','r') as f:
    line=f.readline().strip()
    
    # lineno=0
    score = 0
    completion_scores=[]
    while line:
        corrupt_score=is_corrupt(line.strip())
        if corrupt_score > 0:
            score+=corrupt_score
        else:
            completion_scores.append(complete(line.strip()))
            
        line=f.readline().strip()
    completion_scores=sorted(completion_scores)
    print(len(completion_scores))
    print(completion_scores[int((len(completion_scores)-1)/2)])
    # total = solve(map)
    # print(total)
    testing = 26397
    if score != 26397:
        print('error ',testing,score)
        exit()

map=[]
with open('input','r') as f:
    # with open('test','r') as f:
    line=f.readline().strip()
    
    # lineno=0
    completion_scores=[]
    while line:
        corrupt_score=is_corrupt(line.strip())
        if corrupt_score > 0:
            score+=corrupt_score
        else:
            completion_scores.append(complete(line.strip()))
            
        line=f.readline().strip()
    completion_scores=sorted(completion_scores)
    print(len(completion_scores))
    print(completion_scores[int((len(completion_scores)-1)/2)])
    