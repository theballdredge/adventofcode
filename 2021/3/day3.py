from typing import final
import pprint

g = 0
e = 0
sum = [0,0,0,0,0,0,0,0,0,0,0,0]
avg = [0,0,0,0,0,0,0,0,0,0,0,0]
final_g = [0,0,0,0,0,0,0,0,0,0,0,0]
final_e = [0,0,0,0,0,0,0,0,0,0,0,0]
final_gi = 0
final_ei = 0
count = 0
o2 = []
co2 = []

def common(arr,pos):
    sum=0
    for item in arr:
        sum+=int(item[pos])
    print('common',sum,len(arr),sum/float(len(arr)))
    if sum/float(len(arr))>=.5:
        return 1
    else:
        return 0
def uncommon(arr,pos):
    sum=0
    for item in arr:
        sum+=int(item[pos])
    print('uncommon',sum,len(arr),sum/float(len(arr)))
    if sum/float(len(arr))>=.5:
        return 0
    else:
        return 1

with open('input','r') as f:
    line=f.readline()
    print(len(line))
    while line:
        
        o2.append(line.strip())
        co2.append(line.strip())
        for i in range(0,len(line)-2):
            sum[i] = sum[i] + int(line[i])
        count+=1
        line=f.readline()
    i=0

    for i in range (0,len(sum)):
        expo=len(sum)-1-i
        if sum[i]/float(count)>=.5:
            final_gi+=1*pow(2,expo)
            final_ei+=0*pow(2,expo)
        else:
            final_gi+=0*pow(2,expo)
            final_ei+=1*pow(2,expo)
    print(final_gi,final_ei)
    print(final_ei*final_gi)

    print('\n\npart2')

    common_list=[]
    uncommon_list=[]
    for j in range(0,len(o2[0])):
        common_list.append(common(o2,j))
        uncommon_list.append(uncommon(co2,j))
        # print(len(o2),len(co2))
        # print(common_list)
        # print(uncommon_list)
        
        if len(o2)>1:
            for item in o2[:]:
                if int(item[j])!=int(common_list[j]):
                        o2.remove(item)
        
        if len(co2)>1:
            for item in co2[:]:
                if int(item[j])!=int(uncommon_list[j]):
                        co2.remove(item)
    final_o2_s=[str(item) for item in o2[0]]
    final_co2_s=[str(item) for item in co2[0]]

    final_o2=int(''.join(final_o2_s),2)
    final_co2=int(''.join(final_co2_s),2)

    print(final_o2,final_co2)
    print(final_o2*final_co2)