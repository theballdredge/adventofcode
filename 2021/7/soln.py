def fuelburn(start):
    count=start*(start+1)/2
    return count

def solve(crabarmy):
    fuel=[0]*(max(crabarmy)+1)
    for center in range(min(crabarmy),max(crabarmy)+1):
        fuel[center]=0
        for crab2 in crabarmy:
            fuel[center]+=fuelburn(abs(crab2-center))
    return(min(fuel),fuel.index(min(fuel)))

with open('input','r') as f:
    line=f.readline().strip()
    little_crabarmy=[int(x) for x in line.strip().split(',')]
    total_fuel,index = solve(little_crabarmy)
    print(total_fuel,index,sum(little_crabarmy)/float(len(little_crabarmy)))
    if total_fuel != 168:
        print('error')
        exit()
    
    line=f.readline().strip()
    big_crabarmy=[int(x) for x in line.strip().split(',')]
    total_fuel,index=solve(big_crabarmy)
    print(total_fuel,index,sum(big_crabarmy)/float(len(big_crabarmy)))