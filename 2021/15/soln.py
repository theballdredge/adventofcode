from pprint import pprint
from typing import final
import collections

def setup(file):
    with open(file,'r') as f:
        map=[]
        line=f.readline().strip()
        while line:
            map.append([x for x in line.strip()])
            line=f.readline().strip()
    return map
#p1 test

def path(map):
    minscore=9*len(map)*len(map[0])
    # print(pow(2,len(map)+len(map[0])))
    prev=[0,0]
    # path_next=(map,1,0) 
    for x in range(pow(2,len(map)+len(map[0])-2)):
        # for y in range(pow(2,len(map)+len(map[0]))):
            # print()
            xpos,ypos=prev
            xturns="{0:b}".format(x).rjust(len(map)+len(map[0])-2,'0')
            # print((xturns))
            # yturns="{0:b}".format(y).rjust(len(map[0]),'0')
            # print(xturns,len(map))
            pathscore=0
            for xstep in xturns:
                # for ystep in yturns:
                # print(xstep)
                if int(xstep)==0:
                    if xpos<len(map)-1:
                        xpos+=1
                    else:
                        ypos+=1
                else:
                    if ypos<len(map[0])-1:
                        ypos+=1
                    else:
                        xpos+=1
                # print(xpos,ypos)
                try:
                    pathscore+=int(map[xpos][ypos])
                except IndexError as e:
                    print(xturns,len(xturns),xstep,xpos,ypos)
            # print('newpos', xpos,ypos, xturns)
            if pathscore<minscore:
                minscore=pathscore
            # print(pathscore,minscore)

    print(minscore)
    return minscore

def process(paircount,template):
    unique=[]
    for pair in paircount.keys():
        for c in pair:
            if c not in unique:
                unique.append(c)
    counts={}
    min=0
    max=0
    total=0
    for c in unique:
        counts[c]=0
        for pair in paircount.keys():
            if c == pair[0]:
                counts[c]+=paircount[pair]
            if c == pair[1]:
                counts[c]+=paircount[pair]
        if c in [template[0],template[-1]]:
            counts[c]+=1
        counts[c]=int(counts[c]/2)
        if min == 0:
            min = counts[c]
        if counts[c]<min:
            min = counts[c]
        if counts[c]>max:
            max = counts[c]
        total+=counts[c]
    return max-min

    
def run(file,testing):
    map=setup(file)
    # print(map)
    score=path(map)
    # exit()
    if testing > 0:
        if score != testing:
            print('testing error ',testing,score)
            exit()
        else:
            print('test success')
    else:
        highlist=[0]
        lowlist=[0]
        wronglist=[2437]
        if score in wronglist:
            print('nope',score)
        elif highlist[0] == 0 or score in range(max(lowlist),min(highlist)):
            print('maybe',score)
        else:
            print('nope',score)


run('test',40)

run('input',-1)
exit()
run('test',2188189693529,40)

run('input',-1,40)
