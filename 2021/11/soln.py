from pprint import pprint
def inbounds_points(x,y,octo):
    y_values=[0]
    x_values=[0]
    points_array=[]
    if y != 0:
        y_values.append(-1)
    if y+1 < len(octo[x]):
        y_values.append(1)
    if x != 0:
        x_values.append(-1)
    if x+1 < len(octo):
        x_values.append(1)
    for xa in x_values:
        for ya in y_values:
            if [xa,ya] != [0,0]:
                points_array.append([x+xa,y+ya])
    return points_array

def process(octo):
    count=0
    nextpoints=[]
    processed=[]
    flashed=[]
    for x in range(len(octo)):
        for y in range(len(octo[0])):
            octo[x][y]+=1
    
    for x in range(len(octo)):
        for y in range(len(octo[0])):
            if octo[x][y]>9:
                count+=1
                octo[x][y]=0
                flashed.append([x,y])
                for point in inbounds_points(x,y,octo):
                    if point not in flashed:
                        nextpoints.append(point)
            while nextpoints:
                x_1,y_1 = nextpoints.pop(0)
                processed.append([x_1,y_1])
                
                if [x_1,y_1] not in flashed:
                    octo[x_1][y_1]+=1
                    if octo[x_1][y_1]>9:
                        count+=1
                        octo[x_1][y_1]=0
                        flashed.append([x_1,y_1])
                        for point in inbounds_points(x_1,y_1,octo):
                            if point not in flashed:
                                nextpoints.append(point)
    for point in flashed:
        xf,yf = point

        octo[xf][yf] = 0
    return count

def allflash(octo):
    count=0
    nextpoints=[]
    processed=[]
    flashed=[]
    for x in range(len(octo)):
        for y in range(len(octo[0])):
            octo[x][y]+=1
    
    for x in range(len(octo)):
        for y in range(len(octo[0])):
            if octo[x][y]>9:
                count+=1
                octo[x][y]=0
                flashed.append([x,y])
                for point in inbounds_points(x,y,octo):
                    if point not in flashed:
                        nextpoints.append(point)
            while nextpoints:
                x_1,y_1 = nextpoints.pop(0)
                processed.append([x_1,y_1])
                
                if [x_1,y_1] not in flashed:
                    octo[x_1][y_1]+=1
                    if octo[x_1][y_1]>9:
                        count+=1
                        octo[x_1][y_1]=0
                        flashed.append([x_1,y_1])
                        for point in inbounds_points(x_1,y_1,octo):
                            if point not in flashed:
                                nextpoints.append(point)
    if len(flashed) == len(octo)*len(octo[1]):
        return True
    for point in flashed:
        xf,yf = point

        octo[xf][yf] = 0
    return False

def solve(input):
    return 1

#p1 test
with open('test','r') as f:
    line=f.readline().strip()
    
    lineno=0
    score = 0
    completion_scores=[]
    octo=[]
    focto=[]
    while line:
        octo.append([int(x) for x in line])
        focto.append([int(x) for x in line])
        line=f.readline().strip()
    for t in range(100):
        score+=process(octo)
    print(score)
    testing = 1656
    if score != testing:
        print('testing error ',testing,score)
        exit()
    t=1
    while not allflash(focto):
        t+=1
    print('first allflash',t)
    testing = 195
    if t != testing:
        print('testing error ',testing,t)
        exit()


map=[]
with open('input','r') as f:
    line=f.readline().strip()
    
    lineno=0
    score = 0
    completion_scores=[]
    octo=[]
    focto=[]
    while line:
        octo.append([int(x) for x in line])
        focto.append([int(x) for x in line])
        line=f.readline().strip()
    for t in range(100):
        score+=process(octo)
    print(score)
    t=1
    while not allflash(focto):
        t+=1

    print('first allflash',t)