package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func sum(array []int) int {
	result := 0
	for _, v := range array {
		result += v
	}
	return result
}

func main() {
	var window []int
	file, err := os.Open("./input")
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	prev := -1
	cur := 0
	count := 0
	false_count := 0
	examined := 0
	for scanner.Scan() {
		examined++
		line, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		if len(window) < 3 {
			window = append(window, line)
		} else {
			window = window[1:]
			window = append(window, line)
			cur = sum(window)
			fmt.Println(window)
			if cur > prev {
				// fmt.Println(cur, prev, "increased")
				count++
			} else {
				// fmt.Println(cur, prev)
				false_count++
			}
			prev = cur
		}
	}
	// 1702
	fmt.Println(count, false_count, count+false_count, examined)
}
