package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)

func main() {
	file, err := os.Open("./input")
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	scanner := bufio.NewScanner(file)
	prev := -1
	cur := 0
	count := 0
	false_count := 0
	examined := 0
	for scanner.Scan() {
		examined++
		line, err := strconv.Atoi(scanner.Text())
		if err != nil {
			log.Fatal(err)
		}
		if prev == -1 {
			prev = line
		} else {
			cur = line
			if cur > prev {
				// fmt.Println(cur, prev, "increased")
				count++
			} else {
				// fmt.Println(cur, prev)
				false_count++
			}
			prev = cur
		}
	}
	// 1665
	fmt.Println(count, false_count, count+false_count, examined)
}
