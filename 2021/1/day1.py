prev='na'
count=0
cur=0
with open('input','r') as f:
    line=f.readline()
    while line:
        cur=int(line)
        # print(prev,' ',cur)
        if prev=='na':
            prev=int(line)
        else:
            if cur > prev:
                count=count+1
            prev=int(cur)
            line=f.readline()
print(count)