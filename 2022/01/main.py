with open('input','r') as f:
    cal_list = []
    cal_total = []
    line = f.readline()
    while line:
        if line == '\n':
            cal_total.append(sum(cal_list))
            cal_list = []
        else:
            cal_list.append(int(line))
        line = f.readline()
cal_total.sort(reverse=True)
print(cal_total[0])
print(cal_total[0:3])
print(sum(cal_total[0:3]))
