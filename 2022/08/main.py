def setup(file):
    grid = []
    with open(file,'r') as f:
        line = f.readline().strip()
        while line:
            grid.append(line)
            line=f.readline().strip()
    return grid

def is_visible(grid,x,y):
    n_vis = True
    s_vis = True
    e_vis = True
    w_vis = True
    # print('point',grid[x][y])
    # print('n')
    for i in range(0,x):
        # print(grid[i][y])
        if grid[x][y] <= grid[i][y]:
            n_vis = False
    # print('s')
    for i in range(x+1,len(grid[y])):
        # print(grid[i][y])
        if grid[x][y] <= grid[i][y]:
            s_vis = False
    # print('e')
    for i in range(0,y):
        # print(grid[x][i])
        if grid[x][y] <= grid[x][i]:
            e_vis = False
    # print('w')
    for i in range(y+1,len(grid)):
        # print(grid[x][y], grid[x][i])
        if grid[x][y] <= grid[x][i]:
            w_vis = False
    # print(n_vis, s_vis , e_vis, w_vis)
    return (n_vis or s_vis or e_vis or w_vis)

def scenic_score(grid,x,y):
    n_vis = 0
    s_vis = 0
    e_vis = 0
    w_vis = 0
    orig_h=int(grid[x][y])
    max=orig_h
    prev = 0
    # print('******')
    for i in range(x-1,-1,-1):
        cur = int(grid[i][y])
        n_vis += 1
        if cur >= orig_h:
            break
    max=orig_h
    prev = 0
    for i in range(x+1,len(grid[y])):
        cur = int(grid[i][y])
        s_vis += 1
        if cur >= orig_h:
                break
    max=orig_h
    prev = 0
    for i in range(y-1,-1,-1):
        cur = int(grid[x][i])
        e_vis += 1
        if cur >= orig_h:
                break
    max=orig_h
    prev = 0
    for i in range(y+1,len(grid)):
        cur = int(grid[x][i])
        w_vis += 1
        if cur >= orig_h:
            break
    # if n_vis * s_vis * e_vis * w_vis != 0:
    if x == 42:
        if y == 71:
            print(n_vis, s_vis , e_vis, w_vis, x ,y, orig_h)
            print(n_vis * s_vis * e_vis * w_vis)
    return (n_vis * s_vis * e_vis * w_vis)

def count_visible(grid):
    count = 0
    for x in range(0,len(grid[0])):
        for y in range(0,len(grid)):
            if is_visible(grid,x,y):
                # print('visible',grid[x][y],x,y)
                count += 1
    # count += (2*(len(grid)-1))
    # count += (2*(len(grid[0])-1))
    return count

def max_score(grid):
    max_score = 0
    max_x=0
    max_y=0
    for x in range(0,len(grid[0])):
        for y in range(0,len(grid)):
            score = scenic_score(grid,x,y)
                # print('visible',grid[x][y],x,y)
            if score > max_score:
                max_score = score
                max_x=x
                max_y=y
    # count += (2*(len(grid)-1))
    # count += (2*(len(grid[0])-1))
    print(max_x,max_y)
    return max_score

def run(file,testing,first=True):
    count = 0
    grid=setup(file)


    if first:
        count = count_visible(grid)
    else:
        count = max_score(grid)
        print('h',scenic_score(grid,3,2))
        
    if testing > 0:
        if testing != count:
            print('testing error ',testing,count)
            exit()
        else:
            print('test success')
    else:
        print(count)
run('test',21)

# run('input',-1)
#930 low
# run('input',1538)

run('test',8,False)

run('input',-1,False)
# 3674250 high
#1680 ?
#1470 no
#960 no
#256 low
#240 no
run('input',7068748,False)