def setup(file):
    with open(file,'r') as f:
        return f.readline()

def test(window,length):
    if len(set(window)) == length:
        return True
    else:
        return False

def run(file,testing,first=True):
    transmission=setup(file)
    if first:
        position=0
        window = list(transmission[0:4])
        if not test(window,4):
            for i in range(4,len(transmission)):
                window.pop(0)
                window.append(transmission[i])
                if test(window,4):
                    position=i+1
                    break

    else:
        position=0
        window = list(transmission[0:14])
        if not test(window,14):
            for i in range(14,len(transmission)):
                window.pop(0)
                window.append(transmission[i])
                if test(window,14):
                    position=i+1
                    break
    if testing > 0:
        if position != testing:
            print('testing error ',testing,position)
            exit()
        else:
            print('test success')
    else:
        print(position)

run('test',5)

run('input',-1)

run('input',1210)

run('test2',19,False)

run('input',-1,False)
