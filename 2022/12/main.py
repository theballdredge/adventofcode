import math
def setup(file):
    input = []
    with open(file,'r') as f:
        line = f.readline().strip()
        while line:
            input.append(line)
            line = f.readline().strip()
    return input

def find_start(input):
    for line in input:
        if 'S' in line:
            return input.index(line),line.index('S')


def find_end(input):
    for line in input:
        if 'E' in line:
            return input.index(line),line.index('E')

def valid_moves(input,cx,cy,path):
    movelist=[]
    cur = input[cx][cy]
    if cur == 'S':
        cur = 'a'
    if cx+1 in range(len(input)):
        if input[cx+1][cy] == chr(ord(cur)+1) or input[cx+1][cy] <= cur:
            if [cx+1,cy] not in path:
                movelist.append([cx+1,cy])
    if cx-1 in range (len(input)):
        if input[cx-1][cy] == chr(ord(cur)+1) or input[cx-1][cy] <= cur:
            if [cx-1,cy] not in path:
                movelist.append([cx-1,cy])
    if cy+1 in range(len(input[0])):
        if input[cx][cy+1] == chr(ord(cur)+1) or input[cx][cy+1] <= cur:
            if [cx,cy+1] not in path:
                movelist.append([cx,cy+1])
    if cy-1 in range(len(input[0])):
        if input[cx][cy-1] == chr(ord(cur)+1) or input[cx][cy-1] <= cur:
            if [cx,cy-1] not in path:
                movelist.append([cx,cy-1])
    return movelist


def path(input):
    s_x,s_y = find_start(input)
    e_x,e_y = find_end(input)
    # print(e_x,e_y)
    # print(s_x,s_y)
    input[e_x] = input[e_x][:e_y]+'z'+input[e_x][e_y+1:]
    cx=int(s_x)
    cy=int(s_y)
    decision_pt = []
    paths_finished = []
    paths_going = [[[0,0]]]
    while paths_going:
        print(len(paths_going),len(paths_going[0]))
        path = paths_going.pop(0)
        cx = path[-1][0]
        cy = path[-1][1]
        # print(path)
        moves = valid_moves(input,cx,cy,path)
        # print(moves)
        for move in moves:
            from copy import copy,deepcopy
            newpath = copy(path)
            # print(move)
            newpath.append(move)
            if [newpath[-1][0],newpath[-1][1]] == [e_x,e_y]:
                paths_finished.append(newpath)
            else:
                paths_going.append(newpath)
    print(paths_finished)
    min = len(paths_finished[0])-1
    for path in paths_finished:
        if len(path)-1 <= min:
            # print(path)
            min = len(path)-1
    return min

def run(file,testing,first=True):
    total = 0
    input=setup(file)

    if first:
        print(input)
        total = path(input)
        print(total)
        # exit()

    else:
        monkeys = swarm()
        for m_set in input:
            monkeys.add_monkey(monkey(m_set))
        monkeys.print()
        for i in range(0,rounds):
            print('round',i)
            monkeys.monkey_turn(0)
        monkeys.print()
        monkeys.most_active()
        total=monkeys.monkeybusiness()
        
    if testing > 0:
        if testing != total:
            print('testing error ',testing,total)
            exit()
        else:
            print('test success')
    else:
        print(total)
run('test',31,True)
#138 low
run('input',-1,True)
#930 low
# run('test',10197,20,False)
# run('test',2713310158,10000,False)

# run('input',-1,10000,False)
# run('test',,False)

# run('input',-1,False)
# 3674250 high
#1680 ?
#1470 no
#960 no 
#256 low
#240 no
# run('input',7068748,False)