def setup(file):
    input = []
    with open(file,'r') as f:
        line = f.readline().strip()
        while line:
            input.append(line)
            line=f.readline().strip()
    return input

class processor:
    value = 1
    cmd_running = ''
    cmd_loaded = ''
    stage1 = 0

    def load(self,instr):
        cmd = instr.split(" ")[0]
            # print('new',self.cmd)
        self.cmd_loaded = cmd
        if cmd == 'addx':
            val = int(instr.split(" ")[1])
            # print(val)
            self.stage1 = val

    def exec(self):
        # print(self.value, self.stage1)
        if self.cmd_loaded != '':
            if self.cmd_loaded == 'noop':
                self.cmd_loaded = ''
            elif self.cmd_loaded == 'addx':
                self.cmd_running = self.cmd_loaded
                self.cmd_loaded = ''
        else:
            if self.cmd_running == 'addx':
                # print('running')
                self.value += self.stage1
                self.stage1 = 0
            self.cmd_running = ''

def run(file,testing,first=True):
    instructions=setup(file)
    proc = processor()

    total = 0
    if first:
        proc = processor()
        i = 1
        while instructions:
            # print(proc.cmd_running,proc.stage1)
            if proc.cmd_running == '':
                proc.load(instructions.pop(0))
            if i in [20, 60, 100, 140, 180, 220]:
                # print('signal',i,proc.value,proc.cmd_running,proc.stage1)
                total += (i * proc.value)
            proc.exec()
            i += 1
            # proc.compute()
    else:
        i = 1

        proc = processor()
        line = ['.']*40
        from copy import copy
        render = [copy(line),copy(line),copy(line),copy(line),copy(line),copy(line)]
        while instructions:
            if proc.cmd_running == '':
                proc.load(instructions.pop(0))

            
            frame = [*range(proc.value-1,proc.value+2)]
            proc.exec()
            if (i-1)%40 in frame:
                render[(i)//40][(i-1)%40]="#"
            i += 1
        for y in range(0,len(render)):
            print(''.join(render[y]))
    if testing > 0:
        if testing != total:
            print('testing error ',testing,total)
            # exit()
        else:
            print('test success')
    else:
        print(total)

run('test',13140)

run('input',-1)
#930 low
# run('input',1538)

run('test',8,False)

run('input',-1,False)
