class fsentry:
    name = ''
    meta = ''
    parent = ''
    children = []
    def __init__(self, name, meta, parent):
        self.name = name
        self.meta = meta
        self.parent = parent
        self.children = []
    def printtree(self):
        if self.name == 'root':
            print(self.name,self.meta,self.parent,[x.name for x in self.children])
        else:
            print(self.name,self.meta,self.parent.name,[ x.name for x in self.children])
        for child in self.children:
            child.printtree()
    def dir_total(self):
        total = 0 if self.meta == 'dir' else int(self.meta)
        for child in self.children:
            total += int(child.dir_total())
        return total
    def get_dirs(self):
        dir_list = []
        if self.meta == 'dir':
            dir_list.append(self.name) 
        for child in self.children:
            child_list = child.get_dirs()
            if child_list is not []:
                for item in child_list:
                    dir_list.append(self.name+'/'+item)
        return list(set(dir_list))

def get_node(dir,node):
    path = dir.split('/')
    if len(path) == 1:
        if node.name == path[0]:
            return node
        else:
            return None
    elif node.name == path[0]:
        for child in node.children:
            if child.name == path[1]:
                return get_node('/'.join(path[1:]),child)

def dir_totals(s_node):
    totals = {}
    for dir in s_node.get_dirs():
        node = get_node(dir,s_node)
        totals[dir]=node.dir_total()
    return totals

def exec_cmd(block,pos,root):
    # global pos
    line = block.pop(0)
    if 'cd' in line:
        garb, cmd, arg = line.split(' ')
        if arg == "/":
            pos = root
        elif arg == "..":
            pos = pos.parent
        else:
            childlist = []
            for child in pos.children:
                if child.name == arg:
                    pos = child
                childlist.append(child.name)
            if arg not in childlist:
                newnode = fsentry(arg,'dir',pos)
                pos.children.append(newnode)
                pos = newnode
    elif 'ls' in line:
        for line in block:
            meta, name = line.split(' ')
            childlist = []
            for child in pos.children:
                childlist.append(child.name)
            if name not in childlist:
                newnode = fsentry(name,meta,pos)
                pos.children.append(newnode)
    return pos

def setup(file):
    with open(file,'r') as f:
        return f.read()

def run(file,testing,first=True):
    
    root = fsentry('root','dir',None)
    pos = root
    clearable = 0 
    commands=list(setup(file).split('\n'))
    block = []
    while commands:
        command = commands.pop(0)
        if '$' in command:
            block.append(command)
            while len(commands) > 0 and '$' not in commands[0]:
                block.append(commands.pop(0))
            pos = exec_cmd(block,pos,root)
            block = []
    if first:
        totals=dir_totals(root)
        totals_sorted=sorted(totals.keys())
        for key in totals_sorted:
            if totals[key] < 100000:
                clearable += totals[key]
    else:
        totalsize = 70000000
        needed = 30000000
        rootsize = root.dir_total()
        toreclaim = rootsize-(totalsize-needed)
        print('used',rootsize)
        print('free',totalsize-rootsize)
        print('toreclaim',toreclaim)
        totals=dir_totals(root)
        totals_sorted=sorted(totals.values())
        for i in totals_sorted:
            if int(i) > toreclaim:
                print('smallest dir',i)
                clearable = i
                break
    if testing > 0:
        if testing != clearable:
            print('testing error ',testing,clearable)
            exit()
        else:
            print('test success')
    else:
        print(clearable)
run('test',95437)

run('input',-1)

#1403339 high

run('input',1307902)

run('input',-1,False)

run('input',7068748,False)