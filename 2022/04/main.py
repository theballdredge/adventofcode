def setup(file):
    lines = []
    with open(file,'r') as f:
        line=f.readline().strip()
        while line:
            lines.append(line)
            line=f.readline().strip()
    return lines

def eval_range(s1,e1,s2,e2):
    if s1 >= s2:
        if e1 <= e2:
            return True
    if s2 >= s1:
        if e2 <= e1:
            return True
    return False

def eval_range_any(s1,e1,s2,e2):
    r2=list(range(s2,e2+1))
    r1=list(range(s1,e1+1))
    if set([s1,e1]).intersection(r2):
        return True
    if set([s2,e2]).intersection(r1):
        return True
    return False

def run(file,testing,first=True):
    lines=setup(file)
    count = 0
    for line in lines:
        p1,p2 = line.split(',')
        s1,e1 = p1.split('-')
        s2,e2 = p2.split('-')
        #bleugh
        if first:
            if eval_range(int(s1),int(e1),int(s2),int(e2)):
                count += 1
        else:
            #bleugh
            if eval_range_any(int(s1),int(e1),int(s2),int(e2)):
                count += 1
    if testing > 0:
        if count != testing:
            print('testing error ',testing,count)
            exit()
        else:
            print('test success')
    else:
        print(count)

run('test',2)

run('input',-1)

run('input',547)

run('test',4,False)

run('input',-1,False)
