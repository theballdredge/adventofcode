def score(x,y):
    score = 0
    o_x = ord(x)
    o_y = ord(y)-23
    score += o_y - 64
    if o_x == o_y:
        score += 3
    elif o_x == 65 and o_y == 66:
        score += 6
    elif o_x == 66 and o_y == 67:
        score += 6
    elif o_x == 67 and o_y == 65:
        score += 6
    return score

def score_fixed(x,y):
    score = 0
    o_x = ord(x)
    o_y = ord(y)-23
    if y == 'Y':
        score += 3 + o_x - 64
    if y == 'Z':
        score += 6
        if o_x == 65:
            score += 2
        elif o_x == 66:
            score += 3
        elif o_x == 67:
            score += 1
    if y == 'X':
        if o_x == 65:
            score += 3
        elif o_x == 66:
            score += 1
        elif o_x == 67:
            score += 2
    return score

with open('input','r') as f:
    line=f.readline()
    totalscore = 0
    totalfixscore = 0
    while line:
        x,y =line.split(' ')
        x = x.strip()
        y = y.strip()
        totalscore += score(x,y)
        totalfixscore += score_fixed(x,y)
        line=f.readline()
    print(totalscore)
    print(totalfixscore)
