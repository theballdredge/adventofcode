from copy import copy


def setup(file):
    moves = []
    with open(file,'r') as f:
        line = f.readline().strip()
        while line:
            moves.append(line)
            line=f.readline().strip()
    return moves

def unique(thelist):
    ulist = []
    for item in thelist:
        if item not in ulist:
            ulist.append(item)
    return ulist

def move_head(p,m_dir,m_dis):
    n_x=p[0]
    n_y=p[1]
    if m_dir == 'U':
        n_y += m_dis
    if m_dir == 'D':
        n_y -= m_dis
    if m_dir == 'R':
        n_x += m_dis
    if m_dir == 'L':
        n_x -= m_dis
    return n_x,n_y

def move_tail(p,h):
    n_x=p[0]
    n_y=p[1]
    d_x = h[0] - n_x
    d_y = h[1] - n_y
    if d_x > 1:
        n_x += 1
        if d_y > 0:
            n_y +=1
        elif d_y < 0:
            n_y -=1
    elif d_x < -1:
        n_x -= 1
        if d_y > 0:
            n_y +=1
        elif d_y < 0:
            n_y -=1
    elif d_y > 1:
        n_y += 1
        if d_x < 0:
            n_x -=1
        elif d_x > 0:
            n_x +=1
    elif d_y < -1:
        n_y -= 1
        if d_x > 0:
            n_x +=1
        elif d_x < 0:
            n_x -=1
    return n_x,n_y

def print_grid(moveset):
    maxlist = []
    for move in moveset:
        maxlist.extend(move)
    for y in range(min(maxlist),max(maxlist)):
        for x in range(max(maxlist),(min(maxlist)),-1):
            if [x,y] in moveset:
                print('#', end = '')
            else:
                print('.', end = '')
        print()

def moves(moveset,knots):
    taillist = []
    headmoves = []
    p_x = 0
    p_y = 0
    r = []
    for i in range(0,knots):
        r.append([0,0])
    headmoves.append(copy(r[0]))
    for move in moveset:
        m_dir, m_dis = move.split(' ')
        for i in range(0,int(m_dis)):
            p_x, p_y = move_head(r[0],m_dir,1)
            r[0] = [p_x,p_y]
            headmoves.append(copy(r[0]))
            for i in range(1,knots):
                r[i][0], r[i][1]= move_tail(r[i],r[i-1])
            taillist.append(copy(r[-1]))
    print_grid(taillist)
    return len(unique(taillist))

def run(file,testing,first=True):
    moveset = setup(file)

    if first:
        unique = moves(moveset,2)
        count = unique
    else:
        unique = moves(moveset,10)
        count = unique

    if testing > 0:
        if testing != count:
            print('testing error ',testing,count)
            exit()
        else:
            print('test success')
    else:
        print(count)
run('test',13)

# run('input',-1)
#6252 low
# run('input',1538)


run('test2',36,False)
# 3674250 high
#256 low
#2230, low
run('input',-1,False)
