def setup(file):
    initial = []
    actions = []
    with open(file,'r') as f:
        line = f.readline()
        while line != '\n':
            # 1,5,9,13...
            initial.append(line)
            line = f.readline()
        line = f.readline()
        while line:
            actions.append(line.strip())
            line = f.readline()

    return initial, actions

def encode(initial):
    cols = initial[len(initial)-1].strip().split(' ')
    state = [None] * int(max(cols))
    for line in initial:
        for i in range(1,len(line),4):
            pos = int(i/4)
            if state[pos] == None:
                state[pos]=[]
            if line[i].isalpha():
                state[pos].append(line[i])
    return state

def act(state,qty,start,end):
    for i in range(0,qty):
        state[end].insert(0,state[start].pop(0))
    return state

def act_9001(state,qty,start,end):
    crane_tmp = []
    for i in range(0,qty):
        crane_tmp.append(state[start].pop(0))
    for i in range(0,qty):
        state[end].insert(0,crane_tmp.pop())
    return state

def get_message(state):
    msg = []
    for i in range(0,len(state)):
        msg.append(state[i][0])
    return ''.join(msg)

def run(file,testing,first=True):
    initial,actions=setup(file)
    state=encode(initial)

    if first:
        for action in actions:
            g1,qty,g2,start,g3,end = action.split(' ')
            state = act(state,int(qty),int(start)-1,int(end)-1)
        message = get_message(state)
    else:
        for action in actions:
            g1,qty,g2,start,g3,end = action.split(' ')
            state = act_9001(state,int(qty),int(start)-1,int(end)-1)
        message = get_message(state)
    if testing != '':
        if message != testing:
            print('testing error ',testing,message)
            exit()
        else:
            print('test success')
    else:
        print(message)

run('test','CMZ')

run('input','HNSNMTLHQ')

run('input','')

run('test','MCD',False)

run('input','',False)
