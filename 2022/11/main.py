import math
def setup(file):
    input = []
    with open(file,'r') as f:
        line = ' '
        while f and line:
            monkey = f.readline().strip()
            starting = f.readline().strip()
            operation = f.readline().strip()
            op = operation.split(' ')[4].strip()
            opval = operation.split(' ')[5].strip()
            if opval != 'old':
                opval = int(opval)
            test = int(f.readline().split(' ')[5])
            true_stmt = int(f.readline().split(' ')[-1])
            false_stmt = int(f.readline().split(' ')[-1])
            line = f.readline()
            # print(monkey)
            monkey = int(monkey.split(' ')[1].strip(':'))
            itemlist = starting.split(':')[-1].split(',')
            tuple = {}
            tuple['items'] = [int(x) for x in itemlist]
            tuple['test'] = test
            tuple['op'] = op
            tuple['opval'] = opval
            tuple['true_dst'] = true_stmt
            tuple['false_dst'] = false_stmt
            input.insert(monkey,tuple)
    return input

class monkey:
    items = []
    test = ''
    op = ''
    opval = ''
    true_dst = ''
    false_dst = ''
    xfer_list = []
    inspect_ct = 0
    def __init__(self,input_obj):
        self.items = input_obj['items']
        self.test = input_obj['test']
        self.op = input_obj['op']
        self.opval = input_obj['opval']
        self.true_dst = input_obj['true_dst']
        self.false_dst = input_obj['false_dst']
        inspect_ct = 0
    def print(self):
        print(self.items,self.opval,self.inspect_ct)
        print(self.true_dst,self.false_dst)

class swarm:
    monkeys = []

    def __init__(self):
        self.monkeys = []

    def add_monkey(self,monkey):
        self.monkeys.append(monkey)

    def monkey_turn(self,worries):
        newval = 0
        for monkey in self.monkeys:
            # self.print()
            # monkey.print()
            while monkey.items:
                # print(monkey.items)
                item = monkey.items.pop()
                # print('**',item)
                # print(monkey.opval)
                if monkey.opval == 'old':
                    opval = item
                else:
                    opval = monkey.opval
                if monkey.op == '*':
                    newval = item * opval
                elif monkey.op == '+':
                    newval = item + opval
                if worries==0:
                    newval = newval%self.test_lcm()
                else: 
                    newval = newval//worries
                monkey.inspect_ct += 1
                if int(newval) % int(monkey.test) == 0:

                    self.monkeys[monkey.true_dst].items.append(newval)
                else:
                    self.monkeys[monkey.false_dst].items.append(newval)
    def most_active(self):
        activelist = []
        for monkey in self.monkeys:
            activelist.append(monkey.inspect_ct)
        activelist.sort(reverse=True)
        return activelist[:2]
    
    def monkeybusiness(self):
        first, second = self.most_active()
        return first * second

    def print(self):
        print('**',len(self.monkeys))

        for monkey in self.monkeys:
            monkey.print()
        print('**')
    
    def test_lcm(self):
        testlist = []
        for monkey in self.monkeys:
            testlist.append(monkey.test)
        lcm_val = math.lcm(*testlist)
        # print(lcm_val)
        return lcm_val

def run(file,testing,rounds,first=True):
    total = 0
    input=setup(file)

    if first:
        
        monkeys = swarm()
        for m_set in input:
            monkeys.add_monkey(monkey(m_set))
        monkeys.print()
        for i in range(0,rounds):
            print('round',i)
            monkeys.monkey_turn(3)
        # monkeys.print()
        monkeys.most_active()
        total=monkeys.monkeybusiness()
        # exit()
    else:
        monkeys = swarm()
        for m_set in input:
            monkeys.add_monkey(monkey(m_set))
        monkeys.print()
        for i in range(0,rounds):
            print('round',i)
            monkeys.monkey_turn(0)
        monkeys.print()
        monkeys.most_active()
        total=monkeys.monkeybusiness()
        
    if testing > 0:
        if testing != total:
            print('testing error ',testing,total)
            exit()
        else:
            print('test success')
    else:
        print(total)
run('test',10605,20,True)
#94200 high
run('input',58786,20,True)
#930 low
# run('test',10197,20,False)
run('test',2713310158,10000,False)

run('input',-1,10000,False)
# run('test',,False)

# run('input',-1,False)
# 3674250 high
#1680 ?
#1470 no
#960 no 
#256 low
#240 no
# run('input',7068748,False)