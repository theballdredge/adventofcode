def setup(file):
    lines = []
    with open(file,'r') as f:
        line=f.readline().strip()
        while line:
            lines.append(line)
            line=f.readline().strip()
    return lines

def prioritize(item):
    offset = []
    offset.insert(1,38)
    offset.insert(0,96)
    prio = ord(item) - offset[int(item.isupper())]
    return int(prio)

def find_common(c1,c2):
    for char in c1:
        if char in c2:
            return char

def find_badge(k1,k2,k3):
    for char in k1:
        if char in k2:
            if char in k3:
                return char

def run(file,testing):
    lines=setup(file)
    priototal = 0
    for line in lines:
        middle = int(len(line)/2)
        c1 = line[:middle]
        c2 = line[middle:]
        common = find_common(c1,c2)
        prio = prioritize(common)
        priototal += prio
    
    if testing > 0:
        if priototal != testing:
            print('testing error ',testing,priototal)
            exit()
        else:
            print('test success')
    else:
        print(priototal)

def run2(file,testing):
    lines=setup(file)
    priototal = 0
    i = 0
    for  i in range(0,len(lines),3):
        badge = find_badge(lines[i],lines[i+1],lines[i+2])
        priototal += prioritize(badge)
    
    if testing > 0:
        if priototal != testing:
            print('testing error ',testing,priototal)
            exit()
        else:
            print('test success')
    else:
        print(priototal)

run('test',157)

run('input',7597)

run2('test',70)

run2('input',-1)

