from aocd import data,get_data,submit
import re
data = get_data().splitlines()
# data[0]="Time:      7  15   30"
# data[1]="Distance:  9  40  200"

def calc_dist(time):
    dist_ret=[]
    for x in range(time):
        dist=x*(time-x)
        dist_ret.append([x,dist])
    return dist_ret

time=[]
distance=[]
time=[int(i) for i in re.sub(' +', ' ', data[0].split(":")[1].lstrip()).split(" ")]
distance=[int(i) for i in re.sub(' +', ' ', data[1].split(":")[1].lstrip()).split(" ")]
i=0
racediff=[]
for i in range(len(time)):
    results=calc_dist(time[i])
    count=0
    for result in results:
        if result[1] > distance[i]:
            count+=1
    racediff.append(count)
print(data[1])
print(racediff)
final = 1
for diff in racediff:
    final = final * int(diff)
print(final)
#8073600 wrong
# submit(final)