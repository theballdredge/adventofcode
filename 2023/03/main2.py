from aocd import data,get_data,submit
engine = get_data().splitlines()

def getnumber(x,y):
    xmin = 0
    xmax = len(engine[y])-1
    xstart=x
    xend=x
    while engine[y][xstart].isdigit() and xstart > xmin-1:
        xstart-=1
    # print(xmax,xend,engine[y][xend])
    while engine[y][xend].isdigit() and xend < xmax:
        xend+=1
    if xend==xmax:
        xend+=1
    
    # print(xstart,xend,engine[y][xstart+1:xend])
    return int(engine[y][xstart+1:xend].strip('.')),xstart,xend

def checksurround_gear(x,y):
    xmin = max(0,x-1)
    xmax = min(len(engine[y]),x+2)
    ymin = max(0,y-1)
    ymax = min(len(engine),y+2)
    partlist=[]
                
    # print(engine[max(0,y-1)])
    # print(engine[y])
    # print(engine[min(len(engine)-1,y+1)])
    for x_iter in range(xmin,xmax):
        for y_iter in range(ymin,ymax):
            # print(x_iter,y_iter,engine[y_iter][x_iter])
            if engine[y_iter][x_iter].isdigit():
                # print(x_iter,y_iter,engine[y_iter][x_iter])
                numbertuple=getnumber(x_iter,y_iter)
                if numbertuple not in partlist:
                    partlist.append(numbertuple)
    return partlist

y=0
total=0
for line in engine:
    x=0
    step=0
    while x < len(line):
        step=1
        if x < len(line):
            if line[x]=='*':
                partlist=checksurround_gear(x,y)
                # print(partlist)
                if len(partlist)==2:
                    # print(partlist[0][0],partlist[1][0])
                    total+=partlist[0][0]*partlist[1][0]
        x+=step
    y+=1
print(total)

if total <= 83790292:
    print('low')
# submit(total)
