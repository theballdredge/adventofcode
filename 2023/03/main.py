from aocd import data,get_data,submit
engine = get_data().splitlines()

symbols=[]
for line in engine:
    for char in line:
        if not char.isdigit():
            if not char == '.':
                if not char in symbols:
                    symbols.append(char)

def checksurround(x,y,step):
    xmin = max(0,x-1)
    xmax = min(len(engine[y]),x+step+1)
    ymin = max(0,y-1)
    ymax = min(len(engine),y+2)
    for x_iter in range(xmin,xmax):
        for y_iter in range(ymin,ymax):
            # print(x_iter,y_iter,engine[y_iter][x_iter])
            if engine[y_iter][x_iter] in symbols:
                print(x_iter,y_iter,engine[y_iter][x_iter])
                return True

y=0
total=0
for line in engine:
    x=0
    step=0
    while x < len(line):
        step=1
        if x < len(line):
            if line[x].isdigit():
                if x+1<len(line):
                    if line[x+1].isdigit():
                        step+=1
                        if x+2<len(line):
                            if line[x+2].isdigit():
                                step+=1
                if checksurround(x,y,step):
                    print(len(engine),x,y,step,engine[max(0,y-1)])
                    print(len(engine),x,y,step,engine[y])
                    print(len(engine),x,y,step,engine[min(len(engine)-1,y+1)])
                    print(total,line[x:x+step])
                    total+=int(line[x:x+step])
        x+=step
    y+=1
print(total)

submit(total)
