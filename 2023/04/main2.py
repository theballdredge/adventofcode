from aocd import data,get_data,submit
cards = get_data().splitlines()

total=0
cardcount=[1 for i in range(len(cards))]
x = 0
for card in cards:
    winners,numbers =card.split(':')[1].split('|')
    winlist = [int(i) for i in ' '.join(winners.split()).split(' ')]
    numlist = [int(i) for i in ' '.join(numbers.split()).split(' ')]
    count=0
    for win in winlist:
        if win in numlist:
            count+=1
    for counter in range(1,count+1):
        cardcount[x+counter]+=cardcount[x]
    x+=1
for count in cardcount:
    total += count

print(total)
