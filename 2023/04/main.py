from aocd import data,get_data,submit
cards = get_data().splitlines()

total=0
for card in cards:
    winners,numbers =card.split(':')[1].split('|')
    print(winners.lstrip(' '), numbers)
    winlist = [int(i) for i in ' '.join(winners.split()).split(' ')]
    numlist = [int(i) for i in ' '.join(numbers.split()).split(' ')]
    print(winlist,numlist)
    count=-1
    for win in winlist:
        if win in numlist:
            count+=1
    if count > -1:
        total+=pow(2,count)
print(total)

submit(total)