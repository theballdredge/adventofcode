from aocd import data,get_data,submit
data = get_data()

print(data)

cubes={'red':12,'green':13,'blue':14}
total=0
for game in data.splitlines():
    id = game.split(':')[0].split(' ')[1]
    rounds = game.split(':')[1]
    valid=True
    for round in rounds.split(';'):
        for cubeshow in round.split(','):
            count,color = cubeshow.lstrip().split(' ')
            if int(count) > cubes[color]:
                valid=False
    if valid:
        total=total+int(id)
        
submit(total)
