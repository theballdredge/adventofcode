from aocd import data,get_data,submit
data = get_data()

def power(cubecount):
    return cubecount['red']*cubecount['green']*cubecount['blue']

total=0
for game in data.splitlines():
    id = game.split(':')[0].split(' ')[1]
    rounds = game.split(':')[1]
    valid=True
    cubecount={'red':0,'green':0,'blue':0}
    for round in rounds.split(';'):
        for cubeshow in round.split(','):
            count,color = cubeshow.lstrip().split(' ')
            if cubecount[color]<int(count):
                cubecount[color]=int(count)
    total=total+power(cubecount)
            

        
submit(total)
