from aocd import data,get_data,submit
data = get_data().splitlines()
cards = ['J','2','3','4','5','6','7','8','9','T','Q','K','A']

def maxnotj(hand):
    count = 0
    for card in hand:
        if card != 'J':
            if hand.count(card)>count:
                count=hand.count(card)
    return count

def classify(hand):
    cards = {}
    for card in hand:
        cards[card]=hand.count(card)
    wilds=hand.count('J')
    bestcard=maxnotj(hand)
    cardmax=wilds+bestcard
    handval = 0
    if cardmax == 5:
        handval = 50
    elif cardmax == 4:
        handval = 40
    elif cardmax == 3:
        if 'J' in hand:
            if 2 == list(cards.values()).count(2):
                handval = 32
            else:
                handval = 30
        elif 2 in cards.values():
            handval = 32
        else:
            handval = 30
    elif cardmax == 2:
        if 2 == list(cards.values()).count(2):
            handval = 22
        else:
            handval = 20
    elif cardmax == 1:
        handval = 10
    if 'J' in hand:
        print(hand,cardmax,wilds,handval)
    return cards,handval


handlist={}
handlist[10]=[]
handlist[20]=[]
handlist[22]=[]
handlist[30]=[]
handlist[32]=[]
handlist[40]=[]
handlist[50]=[]
fulllist=[]
for set in data:
    hand,bid = set.split(' ')
    classified,handtype=classify(hand)
    handlist[handtype].append({'hand':hand,'class':classified,'type':handtype,'bid':int(bid)})
for each in handlist.keys():
    handlist[each]= sorted(handlist[each], key=lambda word: [cards.index(c) for c in word['hand']])
    fulllist.extend(handlist[each])

total=0
x=1
for entry in fulllist:
    # print(entry)
    total+=x*entry['bid']
    x+=1

if total >= 252527372:
    print('high')
print(total)
    